var config = {
  port              : 8080, // port at which application is listening
  convoExpiration   : 180,  // how long before a conversations expire, in seconds
  maxTries          : 3,    // maximum # of attempts to asist user before referring to voice line
  maxSuggestedStops : 6,    // maximum # of stops we’ll suggest/probe about
  CARTAvoicePhone   : "(423) 629-1473",  // CARTA voice line
  multiRouteMax     : 2,    // maximum number of routes to list when requesting all predictions for a stop

  exceptions: { // key/value pairs where the key is the matched text and the value is what it should be replaced with
    'hamilton pl': 'hamilton place'
  }
};

module.exports = config;
