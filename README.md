#CARTAgraphy app
CARTAgraphy provides an SMS interface to CARTA’s [bus tracker API](http://bustracker.gocarta.org).

---

Clone the repository, run `npm install`, then `node server.js`.

Use Postman (or similar) to send a POST request to `localhost:8080/api/incoming` with the following key/value pairs* in the request body (x-www-form-urlencoded):

- `Body` : e.g. `When does #4 arrive at Hamilton Mall?`
- `From`   : e.g. `4235550099`

_*Note that `Body` and `From` above follow the Twilio convention. These are interchangeable with `message` and `phone` respectively._

##Git Submodules
This repository uses a git submodule to track sensitive information like API keys in a private repository. After cloning the `cartagraphy` repo, run `git submodule init` and `git submodule update` to clone the necessary submodule.

You’ll need access to the private `mrkp/cartagraphy_keys` repository on Bitbucket. Please email <alfonso@mrkp.co> if you want to contribute to this repository and need access to the keys.

Alternatively, you could create your own keys repository and change the `.gitmodules` file in this repo to point to your own private repository. The following file needs to be at the root of your keys repository:

### carta.js
This file should look like this:

    var carta_key = "your_carta_api_key_here";
    module.exports = carta_key;

Or equivalent.

For information on acquiring an API key, see [this GitHub issue.](https://github.com/gocarta/gtfs/issues/10)

---

CARTAgraphy is published under the [GNU General Public License v2.0](http://choosealicense.com/licenses/gpl-2.0/).

Copyright (C) 2014  Alfonso Gómez-Arzola & Arne Heggestad

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
