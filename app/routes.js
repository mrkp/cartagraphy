var conversation  = require('./modules/conversation.js'),
    messages      = require('./modules/messages.js'),
    hashPhone     = require('./modules/hashPhone.js'),
    twiML         = require('./modules/twiML.js'),
    logger        = require('./modules/loggers/common.js'),
    helpRequest   = require('./modules/helpRequest.js'),
    createSessionObject = require('./modules/createSessionObject.js'),
    predictions   = require('./flows/predictions.js'),
    responses     = require('./templates/responses.js'),
    moment        = require('moment'),
    uuid          = require('node-uuid'),
    async         = require('async')
    ;

module.exports = function (app, express) {
  var router = express.Router(); // get an instance of the express Router

  // middleware to use for all requests
  router.use(function (req, res, next) {
    next(); // make sure we go to the next routes and don't stop here
  });

  // test route to make sure everything is working (accessed at GET /api)
  router.get('/', function (req, res) {
    res.json({ message: 'Alright, alright, alright! Welcome to CARTAgraphy!' });
  });

  // more routes for our API will happen here
  router.route('/incoming')
    .post(function (req, res) {
      // Set response type from the outset:
      // (so we can send responses from now on)
      res.set('Content-Type', 'text/xml');
      // Get information from the request:
      var query, phoneNo, phoneHash;

      // Begin the flow:
      async.waterfall([

        function (proceedToNextFunction) {
          getPhoneHashAndQuery(function () {
            logger.logUser(phoneHash);
            createSessionObject(phoneHash, query, function (err, sessionObj) {
              proceedToNextFunction(null, sessionObj);
            });
          });
        },

        // ====================================================================
        // CHECK IF THIS IS A REQUEST FOR HELP
        function (sessionObj, proceedToNextFunction) {
          helpRequest(sessionObj.query, function (err, needsHelp) {
            if (err) { logger.logErr(err); }

            if (needsHelp) {
              sessionObj.reasonForConvo = 'help request';
              return messages.copyedit(sessionObj, responses.help.predictions, function (err, sessionObj, message) {
                if (err) { logger.logErr(err); }

                res.send(twiML(message));
              });
            }
            proceedToNextFunction(null, sessionObj);
          });
        },

        // ====================================================================
        // SEND REQUEST TO THE PREDICTIONS FLOW
        function (sessionObj) {
          predictions(sessionObj, function (err, message) {
            if (err) { logger.logErr(err); }

            res.send(twiML(message));
          });
        }

      ]);

      var getPhoneHashAndQuery = function (callback) {
        query = req.body.Body || req.body.message;
        phoneNo = req.body.From || req.body.phone;

        if (phoneNo) {
          phoneHash = hashPhone(phoneNo);
          // phoneHash = phoneNo; // FOR TESTING ONLY; REPLACE WITH HASHED PHONE IN PRODUCTION
          phoneNo = '';
        } else {
          logger.logErr('No phone number attached to this request.');
          return res.send(twiML('No phone number attached to this request.'));
        }

        if (!query) {
          logger.logErr('No message attached to this request.');
          return res.send(twiML('No message attached to this request.'));
        }

        // Quick log to the console while we develop…
        if (process.env.NODE_ENV != 'production') {
          console.log(phoneHash + ' is asking: ' + query);
        }

        callback(null);
      }

    });

  // REGISTER OUR ROUTES -------------------------------
  // all of our routes will be prefixed with /api
  app.use('/api', router);
}
