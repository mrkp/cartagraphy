var async         = require('async'),
    moment        = require('moment'),
    uuid          = require('node-uuid'),
    requestParser = require('../modules/requestParser.js'),
    cartaRequests = require('../modules/cartaRequests.js'),
    massageStops  = require('../modules/massageStops.js'),
    logger        = require('../modules/loggers/predictions.js'),
    helpRequest   = require('../modules/helpRequest.js'),
    conversation  = require('../modules/conversation.js'),
    messages      = require('../modules/messages.js'),
    config        = require('../../config.js')
    ;

var predictions = function (sessionObj, responseCallback) {
  async.waterfall([

    // ====================================================================
    // TRY TO FIND A ROUTE NUMBER AND MATCHING STOPS; CREATE SESSION OBJECT
    function (proceedToNextFunction) {
      requestParser(sessionObj, function (err, sessionObj) {
        proceedToNextFunction(err, sessionObj);
      });
    },

    // ====================================================================
    // CHECK IF WE HAVE A ROUTE NUMBER AND NOTHING ELSE
    function (sessionObj, proceedToNextFunction) {
      if (sessionObj.routeNo && sessionObj.querySansBus.search(/^\s*$/) !== -1) {
        return askOrReferUser(sessionObj, function (err, sessionObj, message) {
          logger(sessionObj, message);
          responseCallback(err, message);
        });
      }

      proceedToNextFunction(null, sessionObj);
    },

    // ====================================================================
    // FILTER STOPS BY ROUTE AND CHECK IF THAT GIVES A DEFINITIVE STOP
    function (sessionObj, proceedToNextFunction) {
      if (sessionObj.matchedStops.length <= config.maxSuggestedStops * 2) {
        massageStops.filter(sessionObj, function (err, sessionObj) {

          tryToPredictOrMoveOn(sessionObj, proceedToNextFunction);

        });
      } else {
        // Set the reasonForConvo for logging purposes:
        sessionObj.reasonForConvo = 'more than twelve stops matched';
        return askOrReferUser(sessionObj, function (err, sessionObj, message) {
          logger(sessionObj, message);
          responseCallback(err, message);
        });
      }
    },

    // ====================================================================
    // FIND DUPLICATE STOPS AND CHECK IF THAT GIVES A DEFINITIVE STOP
    function (sessionObj, proceedToNextFunction) {
      massageStops.findDuplicates(sessionObj, function (err, sessionObj) {

        tryToPredictOrMoveOn(sessionObj, proceedToNextFunction);

      });
    },

    // ====================================================================
    // CHECK IF FILTERING CAME BACK WITH ZERO STOPS
    function (sessionObj, proceedToNextFunction) {
      if (sessionObj.routeNo && !sessionObj.filteredStops) {
        // Set reasonForConvo for logging purposes:
        sessionObj.reasonForConvo = 'stops not served by route';
        return askOrReferUser(sessionObj, function (err, sessionObj, message) {
          logger(sessionObj, message);
          responseCallback(err, message);
        });
      }

      proceedToNextFunction(null, sessionObj);
    },

    // ====================================================================
    // STILL MATCHING MORE THAN ONE STOP, ASK OR REFER USER
    function (sessionObj, proceedToNextFunction) {
      // Set the reasonForConvo for logging purposes:
      sessionObj.reasonForConvo = 'more than one stop matched after massaging';
      return askOrReferUser(sessionObj, function (err, sessionObj, message) {
        logger(sessionObj, message);
        responseCallback(err, message);
      });
    }
  ]);

  var tryToPredictOrMoveOn = function (sessionObj, waterfallCallback) {
    if (checkForDefinitiveStop(sessionObj)) {
      return cartaRequests.predict(sessionObj, function (err, sessionObj, message) {
        if (sessionObj.tooManyRoutes) {
          // Looks like we made a prediction on a stop and we got too many routes to include
          sessionObj.reasonForConvo = 'predictions for routes serving this stop too numerous';
          if (sessionObj.convoID) {
            if (sessionObj.tries < config.maxTries) {
              conversation.update(sessionObj);
            } else {
              conversation.remove(sessionObj);
              return messages.referUser(sessionObj, function (err, sessionObj, message) {
                logger(sessionObj, message);
                responseCallback(err, message);
              });
            }
          } else {
            conversation.create(sessionObj);
          }
        } else {
          // Reset reasonForConvo for logging purposes:
          sessionObj.reasonForConvo = '';

          // If this happens to be part of a conversation,
          // remove the conversation from the DB:
          if (sessionObj.convoID) {
            conversation.remove(sessionObj);
          }
        }
        logger(sessionObj, message);
        responseCallback(err, message);
      });
    }

    waterfallCallback(null, sessionObj);
  }

  var checkForDefinitiveStop = function (sessionObj) {
    if (sessionObj.definitiveStop) {
      return true;
    } else {
      return false;
    }
  }

  var askOrReferUser = function (sessionObj, callback) {
    // If this is a conversation and there have been fewer than max allowed number of tries,
    // update the conversation session and probe user for more info:
    if (sessionObj.convoID) {
      if (sessionObj.tries < config.maxTries) {
        return conversation.update(sessionObj, function (err, sessionObj) {
          messages.probeUser(sessionObj, callback);
        });

      // If this is a conversation and we’ve exceeded max allowed number of tries,
      // remove the conversation session and refer user to CARTA live support:
      } else {
        return conversation.remove(sessionObj, function (err, sessionObj) {
          messages.referUser(sessionObj, callback);
        });
      }

    // If this is not a conversation, create a conversation session
    // and probe user for more info:
    } else {
      return conversation.create(sessionObj, function (err, sessionObj) {
        messages.probeUser(sessionObj, callback);
      });
    }
  }
}

module.exports = predictions;
