This directory contains the logs for CARTA Text. Logs are named as <date-of-log>-<content>.log

Usage: <year>-users.log. 
This log stores a hashed version of the incoming phone numbers with how many times each number has used the service. Rotated annually. Comma-separated.

Conversations: <year-month>-convo.log. 
This log stores information relating to every conversation. Conversations and sessions are assigned unique identifiers by the app, so individual conversations can be tracked over multiple sessions. The first session for each conversation establishes that conversation's identifier. Rotated monthly (to help prevent it from becoming too large). Tab-separated.

Errors: <year-month>-errors.log.
This log stores errors identified by the app, including errors in the POST requests from Twilio and database-related errors. Rotated monthly.

Database: <year-month>-mongodb.log. 
This log stores all connection and disconnection events, as well as database-related errors. Rotated monthly. 
