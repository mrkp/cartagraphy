/* jshint quotmark:double */
var responses = {
  predictions: {
    destination: "{amount}{[ #]route}{[ ]type} headed to {destination} {verb}{[ at ]stop} {eta}",
    firstDestination: "{amount} {route}{type} headed to {destination} {verb} at {stop} {eta}",
    secondDestination: "{amount} headed to {destination} {verb} {eta}",
    more: "{amount} more {eta}",
    verbs: {
      present: {
        single: "is arriving",
        multiple: "are arriving"
      },
      future: "will arrive"
    },
    otherRoutes: "{routes} also {serve} this stop, but I have no more predictions."
  },

  probes: {
    routeInfo: "Route #{route}: {routeName}. Tell me your stop and I'll give you predictions.",
    clarifyRoute: "Did you mean {suggestedRoutes}?",
    invalidRoute: "That isn't a valid route. I know about these routes: {validRoutes}. Try one of those.",
    unclearRoute: "Your route number isn't clear to me. Say something like 'bus 4', '#9' or 'Shuttle'.",
    noService: {
      multiStop: "None of the stops I found are served by {route}. Try a different route number or stop.",
      singleStop: "{stopName} isn't served by {route}. Try one of these: {servingRoutes}."
    },
    clarifyStop: "Did you mean {suggestedStops}? Text back the cross street or landmark.",
    tooManyStops: "I've found lots of stops that might match yours. Can you tell me a nearby cross street?",
    tooManyRoutes: "The stop at {stop} is served by routes: {routes}. Tell me which one you want to take."
  },

  referrals: {
    generic: "I'm sorry I'm unable to help you. Please call CARTA at {phone} for assistance.",
    specific: "I'm sorry I'm unable to identify your {reason}. Please call CARTA at {phone} for assistance."
  },

  help: {
    predictions: "Hi, I'm CARTAtext! Tell me your route and stop, and I'll give you predictions. For example: 'bus 4 hamilton place mall', '#1 market and main' or 'broad and 6'."
  }
};

module.exports = responses;
