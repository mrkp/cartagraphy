var moment = require('moment'),
    format = require('string-template'),
    responses = require('../templates/responses.js'),
    config = require('../../config.js')
    ;

var messages = {
  // PROBE USER
  // =============================================================================
  // (asks the user to clarify something)
  probeUser: function (sessionObj, callback) {   // ask the rider a question to clarify their request
    var message;

    // We probe the user at multiple spots in the process, so we use the latest, most narrowed down array of stops:
    var stopsToProbeAbout = sessionObj.culledStops || sessionObj.filteredStops || sessionObj.matchedStops;

    // Figure out what needs to be clarified,
    // construct message accordingly:
    if (sessionObj.routeNo && sessionObj.querySansBus.search(/^\s*$/) !== -1) {
      message = format(responses.probes.routeInfo, {
        route: sessionObj.routeNo,
        routeName: global.routesAndStops[sessionObj.routeNo].rtnm.replace(sessionObj.routeNo + ' ', '').toTitleCase()
      });
    } else if (sessionObj.routeHints) {
      var routeHintsString = '';
      sessionObj.routeHints.forEach(function (hint, i) {
        routeHintsString += i === sessionObj.routeHints.length - 1 ? 'or ' : '';
        routeHintsString += hint;
        routeHintsString += i !== sessionObj.routeHints.length - 1 ? ', ' : '';
      });
      message = format(responses.probes.clarifyRoute, {
        suggestedRoutes: routeHintsString
      });
    } else if (sessionObj.validRoute === false) {
      message = format(responses.probes.invalidRoute, {
        validRoutes: global.validRoutesConvo
      });
    } else if (sessionObj.reasonForConvo === 'stops not served by route') {
      var route = sessionObj.routeNo !== 'SHUTTLE' ? 'bus #' + sessionObj.routeNo : 'the Shuttle';
      if ((sessionObj.matchedStops && sessionObj.matchedStops.length === 1) || sessionObj.culledStops && sessionObj.culledStops.length === 1) {
        var theStop = sessionObj.matchedStops.length === 1 ? sessionObj.matchedStops[0] : sessionObj.culledStops[0];
        var servingRoutes;
        findRoutesServingStop(theStop, sessionObj.duplicateStops, function (err, routesThatServe) {
          servingRoutes = messages.prettifyRoutesList(routesThatServe);
        });
        message = format(responses.probes.noService.singleStop, {
          stopName: global.stopsCollection[theStop].stop_name.toTitleCase(),
          route: route,
          servingRoutes: servingRoutes
        });
      } else {
        message = format(responses.probes.noService.multiStop, {
          route: route
        });
      }
    } else if (stopsToProbeAbout.length <= config.maxSuggestedStops) {
      var stopsHintsString = '';
      stopsToProbeAbout.forEach(function (stop, i) {
        stopsHintsString += i === stopsToProbeAbout.length - 1 ? 'or ' : '';
        stopsHintsString += global.stopsCollection[stop].stop_name.toTitleCase();
        stopsHintsString += i !== stopsToProbeAbout.length - 1 ? ', ' : '';
      });
      // Construct question for user:
      message = format(responses.probes.clarifyStop, {
        suggestedStops: stopsHintsString
      });
    } else if (stopsToProbeAbout.length > config.maxSuggestedStops) {
      // message = 'I’m having trouble identifying your stop. Try to be more specific.';
      message = responses.probes.tooManyStops;
    }
    messages.copyedit(sessionObj, message, function (err, sessionObj, editedMessage) {
      callback(err, sessionObj, editedMessage);
    });
  },

  // REFER USER
  // =============================================================================
  // (tells the user to call CARTA)
  referUser: function (sessionObj, callback) {   // failure condition; refer the rider to the CARTA helpline
    var message;
    if (sessionObj.reasonForConvo === 'route number') {
      message = format(responses.referrals.specific, {
        reason: 'route number',
        phone: config.CARTAvoicePhone
      });
    } else if (sessionObj.reasonForConvo === 'more than one stop matched after massaging' || sessionObj.reasonForConvo === 'more than twelve stops matched') {
      message = format(responses.referrals.specific, {
        reason: 'stop',
        phone: config.CARTAvoicePhone
      });
    } else {
      // Construct message referring user to CARTA live phone support:
      message = format(responses.referrals.generic, {
        phone: config.CARTAvoicePhone
      });
    }
    messages.copyedit(sessionObj, message, function (err, sessionObj, editedMessage) {
      callback(err, sessionObj, editedMessage);
    });
  },

  // CONSTRUCT PREDICTION
  // =============================================================================
  // (turns predictions from CARTA response into a message string)
  constructPrediction: function (sessionObj, sortedPredictions, callback) {
    var now = moment(new Date());

    var constructedMessage = '',
        firstRoute = true,
        reportedRoutes = []
        ;

    // For each route in the predictions…
    for (var route in sortedPredictions) {
      // Check that route is not part of prototype:
      if (sortedPredictions.hasOwnProperty(route)) {
        var firstDestination = true;
        reportedRoutes.push(route);

        // For each destination in this route…
        for (var destination in sortedPredictions[route]) {
          // Check that destination is not part of prototype:
          if (sortedPredictions[route].hasOwnProperty(destination)) {

            var predictions = sortedPredictions[route][destination];
            var stop = global.stopsCollection[predictions[0].stpid].stop_name.toTitleCase();
            var template = responses.predictions.destination;
            var verb = responses.predictions.verbs.future;
            var oneAndMore = false;
            var message, amount, eta;

            // Create a moment for first prediction’s ETA to compare with now:
            var firstPrdtm = Math.floor(predictions[0].eta / 1000),
                predictionTimes = []
                ;

            if (firstPrdtm < 90) { // If the first prediction’s ETA is less than 90s from now:
              amount = 'One';
              eta = 'shortly';
              oneAndMore = true;
              // Now, remove first prediction from array:
              predictions.shift()
            } else {
              // If the first prediction’s ETA is 1m or more from now:
              amount = integerToWord(predictions.length);
            }

            predictions.forEach(function (prd, index) {
              predictionTimes[index] = Math.floor(prd.eta / 60000) + 'm';
            });

            message = format(template, {
              amount: amount,
              route: route,
              type: amount === 'One' ? 'bus' : 'buses',
              destination: destination,
              verb: verb,
              stop: firstRoute ? stop : null,
              eta: eta ? eta : 'in ' + predictionTimes.join(', ')
            });

            if (oneAndMore && predictions.length !== 0) {
              // Lump the rest of the predictions together:
              message += '; '
              message += format(responses.predictions.more, {
                amount: integerToWord(predictions.length).toLowerCase(),
                eta: 'in ' + predictionTimes.join(', ')
              });
            }

            // Add period and space to end of message, to separate from next destination/route:
            constructedMessage += message + '. ';

            // And we’re done constructing the message for this destination!
            // =============================================================================

            // Make sure the next destination doesn’t repeat the route # and stop name:
            firstDestination = false;
            firstRoute = false;
          }
        }
      }
    }

    if (!sessionObj.routeNo) {
      findRoutesServingStop(sessionObj.definitiveStop, sessionObj.duplicateStops, function (err, routesThatServe) {
        var filteredRoutesThatServe = [];
        routesThatServe.forEach(function (servingRoute) {
          if (reportedRoutes.indexOf(servingRoute) === -1) {
            filteredRoutesThatServe.push(servingRoute);
          }
        });

        if (filteredRoutesThatServe.length > 0) {
          var additionalRoutes = messages.prettifyRoutesList(filteredRoutesThatServe);
          constructedMessage += format(responses.predictions.otherRoutes, {
            routes: additionalRoutes,
            serve: filteredRoutesThatServe.length === 1 ? 'serves' : 'serve'
          });
        }
      });
    }
    // All predictions for all destinations have been added!
    // Remove trailing period and space:
    var regexp = new RegExp(/. $/);
    constructedMessage = constructedMessage.replace(regexp, '');

    messages.copyedit(sessionObj, constructedMessage, callback);
  },

  // CONSTRUCT ERROR MESSAGE
  // =============================================================================
  // (turns error message from CARTA Prediction response into a message string)
  constructError: function (sessionObj, response, callback) {
    var message;
    if (response.error[0].rt) {
      message = response.error[0].msg
                  + ' for route #'
                  + response.error[0].rt
                  + ' at '
                  + global.stopsCollection[response.error[0].stpid].stop_name.toTitleCase();
    } else {
      message = response.error[0].msg;
    }

    messages.copyedit(sessionObj, message, callback);
  },

  tooManyRoutes: function (sessionObj, callback) {
    var stop = sessionObj.definitiveStop,
        duplicateStops = sessionObj.duplicateStops
        ;

    findRoutesServingStop(stop, duplicateStops, function (err, routesThatServe) {
      var routesString = messages.prettifyRoutesList(routesThatServe);
      var message = format(responses.probes.tooManyRoutes, {
        stop: global.stopsCollection[stop].stop_name.toTitleCase(),
        routes: routesString
      });

      sessionObj.tooManyRoutes = true;
      messages.copyedit(sessionObj, message, callback);
    });
  },

  // COPYEDIT MESSAGE
  // =============================================================================
  // (copyedit message to make it cleaner and more readable)
  copyedit: function (sessionObj, message, callback) {
    // replace stop numbers with stop names
    var stopPattern = /(.*)stop \#(\d*)(\D|$)/,
        editedMessage
        ;

    if (message.search(stopPattern) >= 0) {
      var stop = message.replace(stopPattern, '$2');
      var stopName = global.stopsCollection[stop].stop_name.toTitleCase();
      editedMessage = message.replace(stopPattern, '$1' + stopName);
    } else {
      editedMessage = message;
    }
    // remove improperly capitalized At or And
    var badCaps = /( )A(t |nd )/g;
    editedMessage = editedMessage.replace(badCaps, '$1a$2');
    editedMessage = editedMessage.replace(/Near\s/g, 'near ');
    // remove any stray + symbols from route directions
    editedMessage = editedMessage.replace(/\s\+\s/g, ' and ');
    // replace route 33 and 34 with their shuttle names
    editedMessage = editedMessage.replace(/(route )?\#33( buse*(s)?)?/g, 'Downtown Shuttle$3');
    editedMessage = editedMessage.replace(/(route )?\#34( buse*(s)?)?/g, 'North Shore Shuttle$3');
    // for northshore shuttle, replace 'Tremont and Frazier' destination
    if (editedMessage.search(/North Shore Shuttle/g) > 0) {
      editedMessage = editedMessage.replace(/Tremont and Frazier/, 'Tremont and Frazier (Northshore Garage)');
    }

    // check if street names match; if they do, drop the first one
    var duplStCk = /(at.*)\s(St|Ave|Rd|Dr)(\sand .*\s)(St|Ave|Rd|Dr)/;
    if (editedMessage.replace(duplStCk, '$2') === editedMessage.replace(duplStCk, '$4')) {
      editedMessage = editedMessage.replace(duplStCk, '$1$3$4');
    }

    if (typeof (callback) === 'function') {
      callback(null, sessionObj, editedMessage);
    }
  },

  // SORT AND PRETTIFY LIST OF ROUTES
  // =============================================================================
  // (takes array of routes and returns a sorted, pretty, colloquial list of routes)
  prettifyRoutesList: function (routeArray) {
    var routeString = routeArray.slice();
    // Add padding before the single-digit routes
    routeString.forEach(function (route, index) {
      routeString[index] = route.replace(/^(\d)$/, '0$1');
    });
    routeString = routeString.sort()
                    .join(', #')
                    .replace(/0(\d)/g, '$1')
                    .replace(/\, (\#\d+)$/, ' and $1')
                    .replace(/(\,| and)? (\#33(\,| and) \#34)/, '$3 Shuttle')
                    .replace(/(\,| and)? (\#33|\#34)/, '$1 Shuttle'); // replace routes 33 and 34 with Shuttle
    routeString = '#' + routeString;

    return routeString;
  }
}

// =============================================================================
// ||
// ||     Other utilities for use across multiple cartaRequests methods:
// ||
// =============================================================================

var findRoutesServingStop = function (stop, duplicateStops, callback) {
  var routesThatServe = [];

  var checkRoutes = function (stopToTest) {
    for (var route in global.busroutes) {
      if (global.busroutes.hasOwnProperty(route)) {
        if (global.busroutes[route].indexOf(parseInt(stopToTest)) !== -1
            && routesThatServe.indexOf(route) === -1
            && route !== 'SHUTTLE') {
          routesThatServe.push(route);
        }
      }
    }
  }

  checkRoutes(stop);

  if (duplicateStops && duplicateStops[stop]) {
    duplicateStops[stop].stops.forEach(function (dupeStop) {
      checkRoutes(dupeStop);
    });
  }

  callback(null, routesThatServe);
}

// INTEGER-TO-WORD
// =============================================================================
// (turns any number between 0 and 19 into a word; useful to avoid confusion
//  with bus route numbers, e.g. “Two #4 buses”, as opposed to “2 #4 buses”)
var integerToWord = function (integer) {
  var units = new Array ('Zero', 'One', 'Two', 'Three', 'Four', 'Five', 'Six', 'Seven', 'Eight', 'Nine', 'Ten', 'Eleven', 'Twelve', 'Thirteen', 'Fourteen', 'Fifteen', 'Sixteen', 'Seventeen', 'Eighteen', 'Nineteen');
  var theword = '';
  var started;

  if (integer > 19) { return 'Lots of'; }
  if (integer === 0) { return units[0]; }

  for (var i = 1; i < 20; i ++) {
    if (integer === i) {
      theword += (started ? units[i].toLowerCase() : units[i]);
    }
  }

  return theword;
}

module.exports = messages;
