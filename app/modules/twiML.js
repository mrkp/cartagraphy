var xml2js = require('xml2js');

var twiML = function (message) {
  var xmlBuilder = new xml2js.Builder();
  var responseObj = { Response: { Message: message } };
  var response = xmlBuilder.buildObject(responseObj);
  return response;
}

module.exports = twiML;
