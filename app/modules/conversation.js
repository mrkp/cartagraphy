var mongoose = require('mongoose'),
    PredictionConvos = require('../models/predictionConvoSchema.js'),
    twiML = require('./twiML.js'),
    moment = require('moment'),
    format = require('string-template'),
    responses = require('../templates/responses.js'),
    config = require('../../config.js')
    ;

var conversation = {

  check: function (phoneHash, callback) {
    var parent = this;
    PredictionConvos.findOne({ phoneHash: phoneHash }, function (err, convo) {
      if (err) { console.log(err); }
      if (convo) {
        var now = moment(new Date());
        var lastConvoTime = moment(convo.toObject().startTime);
        if (now.diff(lastConvoTime, 'seconds') <= config.convoExpiration) {
          if (typeof (callback) === 'function') {
            callback(null, convo);
          }
        } else {
          if (typeof (callback) === 'function') {
            conversation.remove(convo.toObject(), function (err) {
              callback(null, false);
            })
          }
        }
      } else {
        if (typeof (callback) === 'function') {
          callback(null, false);
        }
      }
    });
  },

  create: function (sessionObj, callback) {
    sessionObj.convoID = sessionObj.sessionID;
    // Create a document to keep track of this conversation
    var currentConvo = new PredictionConvos(sessionObj);

    currentConvo.save(function (err) {
      if (typeof (callback) === 'function') {
        callback(err, sessionObj);
      }
    });
  },

  update: function (sessionObj, callback) {
    // Update document to keep track of this conversation
    PredictionConvos.update({ phoneHash: sessionObj.phoneHash }, sessionObj, function (err, numAffected) {
      if (typeof (callback) === 'function') {
        callback(err, sessionObj);
      }
    });
  },

  remove: function (sessionObj, callback) {
    PredictionConvos.findOneAndRemove({ phoneHash: sessionObj.phoneHash }, function (err) {
      if (typeof (callback) === 'function') {
        callback(err, sessionObj);
      }
    });
  }
}

module.exports = conversation;
