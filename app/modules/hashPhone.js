var crypto     = require('crypto'),
    iterations = 1000,
    keylen     = 24, // bytes
    salt       = require('../keys/cartagraphy.js')
    ;

var hashPhone = function (phoneNo) {
  return crypto.pbkdf2Sync(phoneNo, salt, iterations, keylen).toString('hex');
}

module.exports = hashPhone;
