var massageStops = {

  filter: function (sessionObj, callback) {
    if (!sessionObj.routeNo) {
      return callback(null, sessionObj);
    }
    // Define filteredStops as an array if it is still null:
    sessionObj.filteredStops = [];

    sessionObj.matchedStops.forEach(function (stop) {
      if (global.busroutes[sessionObj.routeNo].indexOf(parseInt(stop)) >= 0) {
        sessionObj.filteredStops.push(stop);
      }
    });

    if (sessionObj.filteredStops.length === 1) {
      sessionObj.definitiveStop = sessionObj.filteredStops[0];
    } else if (sessionObj.filteredStops.length === 0) {
      sessionObj.filteredStops = null;
      sessionObj.definitiveStop = null;
    }

    if (typeof (callback) === 'function') {
      callback(null, sessionObj);
    }
  },

  findDuplicates: function (sessionObj, callback) {
    // Define duplicateStops as an array if it is still null:
    sessionObj.duplicateStops = {};

    // Set initial value of culledStops to filteredStops (or, in lieu of that, matchedStops):
    sessionObj.culledStops = sessionObj.filteredStops ?
                              sessionObj.filteredStops.slice() :
                              sessionObj.matchedStops.slice();

    var rawStopNames = [];
    var checkedNames = [];
    sessionObj.culledStops.forEach(function (stop) {
      rawStopNames[stop] = global.stopsCollection[stop].stop_name;
    });

    rawStopNames.forEach(function (stopName, index) {
      var duplicatePositions = [];
      if (checkedNames.indexOf(stopName) < 0) {
        var possibleDupe = rawStopNames.indexOf(stopName);
        while (possibleDupe !== -1) {
          duplicatePositions.push(possibleDupe.toString());
          possibleDupe = rawStopNames.indexOf(stopName, possibleDupe + 1);
        }
        if (duplicatePositions.length > 1) {
          duplicatePositions.splice(0, 1);
          sessionObj.duplicateStops[index] = {
            name: stopName,
            stops: duplicatePositions
          };
          duplicatePositions.forEach(function (foundPosition) {
            sessionObj.culledStops.splice(sessionObj.culledStops.indexOf(foundPosition), 1);
          });
        }
        checkedNames.push(stopName);
      }
    });

    if (sessionObj.culledStops.length === 1 && (!sessionObj.routeNo || sessionObj.filteredStops)) {
      sessionObj.definitiveStop = sessionObj.culledStops[0];
    }

    // If no duplicate stops found, revert duplicateStops back to null:
    // (so that it returns false when checked)
    if (sessionObj.duplicateStops.length === 0) {
      sessionObj.duplicateStops = null;
    }

    if (typeof (callback) === 'function') {
      callback(null, sessionObj);
    }
  }

}

module.exports = massageStops;
