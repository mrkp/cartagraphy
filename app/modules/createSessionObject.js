var conversation = require('./conversation.js'),
    queryCleanup = require('./queryCleanup.js'),
    moment = require('moment'),
    uuid = require('node-uuid')
    ;

var createSessionObject = function (phoneHash, query, callback) {
  conversation.check(phoneHash, function (err, convo) {
    var sessionID = uuid.v4(),
        sessionObj = {}
        ;
    if (convo) {
      sessionObj = convo.toObject();
      sessionObj.startTime = moment(new Date()).toDate();
      // create a new session ID (note: sessionID is not stored in the db)
      sessionObj.sessionID = sessionID;
      // Update the object query to the latest version (adding newer query):
      sessionObj.query = sessionObj.querySansBus ? sessionObj.querySansBus + ' ' + query : query;
      // Update the number of tries:
      sessionObj.tries++;
      // Reset routeHints & tooManyRoutes: (No need to include last session’s hints in this session’s logging.)
      sessionObj.routeHints = null;
      sessionObj.tooManyRoutes = null;
      // Remove _id property because mongo is having non-a-that:
      delete sessionObj._id;
    } else {
      sessionObj = {
        phoneHash: phoneHash,
        routeNo: null,
        routeHints: null,
        querySansBus: null,
        definitiveStop: null,
        matchedStops: null,
        filteredStops: null,
        culledStops: null,
        duplicateStops: null,
        reasonForConvo: '',
        tries: 1,
        query: query,
        startTime: moment(new Date()).toDate(),
        convoStartTime: moment(new Date()).toDate(),
        convoID: null, // this will be set to the same value as the sessionID if needed
        sessionID: sessionID,
        tooManyStops: null
      }
    }

    queryCleanup(sessionObj.query, function (err, cleanQuery) {
      sessionObj.query = cleanQuery;
      callback(null, sessionObj);
    });
  });
}

module.exports = createSessionObject;
