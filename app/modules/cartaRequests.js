var apiObj = {
  key: require('../keys/carta.js'),
  host: process.env.APIHOST || 'bustracker.gocarta.org',
  port: process.env.APIPORT || 80
}

var http   = require('http'),
    xml2js = require('xml2js'),
    moment = require('moment'),
    bustime = require('bustime')(apiObj),
    twiML  = require('./twiML.js'),
    conversation = require('./conversation.js'),
    messages = require('./messages.js'),
    format = require('string-template'),
    responses = require('../templates/responses.js'),
    config = require('../../config.js')
    ;

// Create toTitleCase() method for string manipulation.
// (why this doesn’t already exist will forever elude me)
String.prototype.toTitleCase = function () {
    return this.replace(/\w+\/?/g, function (txt) {return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
};

// =============================================================================
// ||
// ||     Requests made to CARTA API live inside the cartaRequests object
// ||
// =============================================================================

var cartaRequests = {
  // PREDICT BUS ARRIVALS
  // =============================================================================
  predict: function (sessionObj, callback) {
    var requestType = '/getpredictions',
        predictions
        ;

    var routes = '';
    if (sessionObj.routeNo === 'SHUTTLE') {

      global.shuttleRoutes.forEach(function (routeID, i) {
        routes += routeID;
        routes += i === global.shuttleRoutes.length - 1 ? '' : ',';
      });

    } else {
      if (sessionObj.routeNo) {
        routes = sessionObj.routeNo;
      }
    }

    var stops = sessionObj.definitiveStop;
    stops += sessionObj.duplicateStops && sessionObj.duplicateStops[sessionObj.definitiveStop] ?
              ',' + sessionObj.duplicateStops[sessionObj.definitiveStop].stops :
              '';
    var reqObj = {
      rt: routes,
      stpid: stops,
      services: {
        calculateETA: true
      }
    }

    bustime.predictions(reqObj, function (err, result) {
      if (err) { throw err; }

      if (result.prd) {
        predictions = result.prd;
        var sortedPredictions = sortPredictions(predictions);
        if (Object.keys(sortedPredictions).length > config.multiRouteMax) {
          return messages.tooManyRoutes(sessionObj, callback);
        }
        messages.constructPrediction(sessionObj, sortedPredictions, callback);
      } else {
        messages.constructError(sessionObj, result, callback);
      }
    });
  }
}

// =============================================================================
// ||
// ||                     Prediction-specific utilities
// ||
// =============================================================================

// SORT PREDICTIONS BY DIRECTION
// =============================================================================
// (puts predictions into an object where each destination is a key whose value
//  is an array of predictions; used to construct a better response message)
var sortPredictions = function (predictions) {
  var sortedPredictions = {};
  predictions.forEach(function (prediction, i) {
    // Create property for this route if it doesn’t already exist:
    if (!sortedPredictions[prediction.rt]) {
      sortedPredictions[prediction.rt] = {};
    }

    if (!sortedPredictions[prediction.rt][prediction.des.toTitleCase()]) {
      sortedPredictions[prediction.rt][prediction.des.toTitleCase()] = [ prediction ];
    } else {
      sortedPredictions[prediction.rt][prediction.des.toTitleCase()].push(prediction);
    }
  });
  return sortedPredictions;
}

module.exports = cartaRequests;
