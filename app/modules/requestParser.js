// Dependencies:
var compare = require('./comparePredictions.js'),
    routeValidator = require('./routeValidator.js'),
    moment = require('moment'),
    uuid = require('node-uuid')
    ;

var routePattern = new RegExp(/(route|rt|bus|line|the|#|number|no)\.* *(\d+)([a-zA-Z]?)/i),
    routePatternNoIdentifier = new RegExp(/(^|\s)(\d+)([a-zA-Z]?)/i),
    namedPattern = new RegExp(/(^|\s+)(shuttle|shut)($|\s)/i)
    ;

var requestParser = function (sessionObj, callback) {
  // Update the object query to the latest version (adding newer query):
  sessionObj.querySansBus = sessionObj.query.replace(routePattern, '');

  findRoute(sessionObj, function (err, detectedRoute) {
    if (err) { throw err; }

    if (sessionObj.convoID) {
      sessionObj.routeNo = detectedRoute ? detectedRoute : sessionObj.routeNo;
    } else {
      sessionObj.routeNo = detectedRoute;
    }
    findStops(sessionObj.querySansBus, function (err, possibleStops) {
      if (err) { throw err; }

      compare(possibleStops, function (err, topStops) {
        if (err) { throw err; }

        sessionObj.matchedStops = topStops;
        sessionObj.definitiveStop = topStops.length === 1 ? topStops : null;
      });

    });

  });

  routeValidator(sessionObj, function (err, sessionObj) {
    callback(err, sessionObj);
  });
}

var findRoute = function (sessionObj, callback) {
  var matchResults = sessionObj.query.match(routePattern),
      matchShuttle,
      detectedRoute
      ;

  // If regex doesn’t find a match, and there is a prior conversation
  // where hints were offered, then we try the regex without an identifier:
  if (!matchResults && sessionObj.convoID && sessionObj.routeHints) {
    matchResults = sessionObj.query.match(routePatternNoIdentifier);
  }

  // If neither regex finds a match, and there is no prior routeNo
  if (!matchResults && (!sessionObj.convoID || !sessionObj.routeNo)) {
    matchShuttle = sessionObj.query.match(namedPattern);
    if (matchShuttle && matchShuttle[2]) {
      detectedRoute = 'shuttle';
    } else {
      detectedRoute = null;
    }

  // If we found a match and there is a modifier (letter after route number):
  } else if (matchResults && matchResults[3]) {

    detectedRoute = matchResults[2] + matchResults[3];

  // If we found match and there is no modifier:
  } else if (matchResults) {
    detectedRoute = matchResults[2];

  // If we didn’t find a route number:
  } else {
    detectedRoute = null;
  }

  callback(null, detectedRoute);
}

var findStops = function (query, callback) {
  addOrdinals(query, function (result) {
    callback(null, global.stopsClassifier.getClassifications(result));
  });
}

var addOrdinals = function (query, callback) {
  // add ordinals to street numbers if needed
  // currently adds ordinals to the end of all numbers; would be better to match only 1 and 2-digit numbers
  query = query.replace(/(^1|\s1)(1|2|3)(\s|$)/g, '$1$2th$3');  // handle 11th, 12th, 13th
  query = query.replace(/(^|\s)([0-9]?)0(\s|$)/g, '$1$20th$3');
  query = query.replace(/(^|\s)([0-9]?)1(\s|$)/g, '$1$21st$3');
  query = query.replace(/(^|\s)([0-9]?)2(\s|$)/g, '$1$22nd$3');
  query = query.replace(/(^|\s)([0-9]?)3(\s|$)/g, '$1$23rd$3');
  query = query.replace(/(^|\s)([0-9]?)([4-9])(\s|$)/g, '$1$2$3th$4');

  callback(query);
}

module.exports = requestParser;
