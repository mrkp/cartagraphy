var exceptions = require('../../config').exceptions;

var queryCleanup = function (query, callback) {
  query = query.replace(/\s+/g, ' ');
  queryExceptions(query, function (err, cleanQuery) {
    query = cleanQuery;
    callback(null, cleanQuery);
  });
}

var queryExceptions = function (query, callback) {
  var cleanQuery = query;
  for (var exception in exceptions) {
    if (exceptions.hasOwnProperty(exception)) {
      cleanQuery = cleanQuery.replace(exception, exceptions[exception])
    }
  }

  callback(null, cleanQuery);
}

module.exports = queryCleanup;
