// dependencies
var moment = require('moment')
    ;

var helpPattern = new RegExp(/(\s|^)(HELP)\s*(\?|!)*(\s|$)/gi);

var check = function (query, callback) {
  var needsHelp = false;
  if (query.search(helpPattern) >= 0) {
    needsHelp = true;
  }
  if (typeof (callback) === 'function') {
    callback(null, needsHelp);
  }
}

module.exports = check;
