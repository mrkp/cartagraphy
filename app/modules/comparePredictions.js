var fs = require('fs');

module.exports = function (possibleStops, callback) {
	var len = possibleStops.length;
	var valueMax = possibleStops[0].value;
	var valueMin = possibleStops[len - 1].value;
	var valueRatio = Math.round(valueMax / valueMin);
	var topStops = [];
	var tempMax = valueMax;
	var k = 0;
	var scoreArray = []
	for (var i = 0; i < len; i ++) {
		if (possibleStops[i].value === valueMax) {
			topStops.push(possibleStops[i].label);
		}
		if (possibleStops[i].value === tempMax) {
			k ++;
		}
		if (possibleStops[i].value !== tempMax) {
			scoreArray.push([ k, tempMax ]);
			k = 0;
			tempMax = possibleStops[i].value;
		}
	}
	scoreArray.push([ k + 1, tempMax ]);
	if (typeof (callback) === 'function') {
		callback(null, topStops);
	}
}
