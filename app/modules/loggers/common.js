var fs = require('fs'),
    moment = require('moment'),
    csv = require('csv-to-json'),
    csvConfig = { console: false }
    ;

exports.logErr = function (message) {
  var logtime = moment(new Date());
  var log = logtime.toDate() + ': ' + JSON.stringify(message) + '\n';
  var logfile = 'app/logs/' + String(moment().year()) + '-' + String(moment().month() + 1).replace(/^(\d)$/, '0$1') + '-err.log';
  fs.appendFileSync(logfile, log);
}

exports.logConnection = function (message) {
  var logtime = moment(new Date());
  var log = logtime.toDate() + ': ' + JSON.stringify(message) + '\n';
  var logfile = 'app/logs/' + String(moment().year()) + '-' + String(moment().month() + 1).replace(/^(\d)$/, '0$1') + '-mongodb.log';
  fs.appendFileSync(logfile, log);
}

exports.logUser = function (phoneHash) {
  var logfile = 'app/logs/' + String(moment().year()) + '-users.log';
  var users;
  try {
    users = csv.parse(logfile, csvConfig);
  } catch (err) {
    fs.writeFileSync(logfile, 'userID,uses');
    users = csv.parse(logfile, csvConfig);
  }

  // try to find the current phoneHash in the users object; if it exists, increment the counter; if not, add it with a counter of 1
  var userIndex = -1;
  for (var i = 0; i < users.length; i++) {
    if (users[i].userID === phoneHash) {
      userIndex = i;
      break;
    }
  }
  if (userIndex >= 0) {
    users[userIndex].uses++;
  } else {
    users.push({ userID: phoneHash, uses: 1 });
  }
  // re-sort the user list by number of uses (decreasing) to help protect our users' privacy
  users = users.sort(function (a, b) {
    return b.uses - a.uses;
  });
  var userlog = 'userID,uses';
  for (var j = 0; j < users.length; j++) {
    userlog += '\n' + users[j].userID + ',' + users[j].uses;
  }
  fs.writeFileSync(logfile, userlog);
}
