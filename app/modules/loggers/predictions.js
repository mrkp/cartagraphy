var fs = require('fs'),
    moment = require('moment'),
    csv = require('csv-to-json')
    ;

var predictionsLogger = function (sessionObj, message) {
  var logtime = moment(new Date());
  var timeDiff = logtime.diff(sessionObj.startTime);
  var totalTimeDiff = logtime.diff(sessionObj.convoStartTime);
  var totalTimeDiffString;
  if (totalTimeDiff < 1000) {
    totalTimeDiffString = totalTimeDiff + ' ms'; // int
  } else if (totalTimeDiff < 60000) {
    totalTimeDiffString = totalTimeDiff / 1000 + ' sec'; // float with up to three digits
  } else {
    totalTimeDiffString = totalTimeDiff / 60000;
    totalTimeDiffString = Math.round(totalTimeDiffString * 1000) / 1000; // float with up to three digits
    totalTimeDiffString += ' min';
  }
  var matchedStops;
  if (sessionObj.matchedStops) {
    if (sessionObj.matchedStops.length > 6) {
      matchedStops = 'Greater than 6 matched stops'
    } else {
      matchedStops = sessionObj.matchedStops.join(', ');
    }
  }
  var filteredStops;
  if (!sessionObj.filteredStops) {
    filteredStops = JSON.stringify(sessionObj.filteredStops);
  } else {
    filteredStops = sessionObj.filteredStops.join(', ');
  }
  var culledStops;
  if (!sessionObj.culledStops) {
    culledStops = JSON.stringify(sessionObj.culledStops);
  } else {
    culledStops = sessionObj.culledStops.join(', ');
  }

  // one log file per month (note: moment()'s months are 0-11)
  var logfile = 'app/logs/' + String(moment().year()) + '-' + String(moment().month() + 1).replace(/^(\d)$/, '0$1') + '-convos.log';

  var headers = 'Conversation ID\tSession ID\tConversation Start Time\tSession Start Time\tSession Length\tTries\tTotal Conversation Length\tQuery\tOur Response\tRoute No\tQuery Sans Bus\tDefinitive Stop\tMatched Stops\tFiltered Stops\tCulled Stops\tDuplicate Stops\tReason for Conversation';
  var log = '\n'
          + sessionObj.convoID + '\t'
          + sessionObj.sessionID + '\t'
          + sessionObj.convoStartTime + '\t'
          + sessionObj.startTime + '\t'
          + timeDiff + '\t'
          + sessionObj.tries + '\t'
          + totalTimeDiffString + '\t'
          + sessionObj.query + '\t'
          + message + '\t'
          + sessionObj.routeNo + '\t'
          + sessionObj.querySansBus + '\t'
          + sessionObj.definitiveStop + '\t'
          + matchedStops + '\t'
          + filteredStops + '\t'
          + culledStops + '\t'
          + JSON.stringify(sessionObj.duplicateStops) + '\t'
          + sessionObj.reasonForConvo + '\t';

  fs.exists(logfile, function (exists) {
    if (!exists) {
      log = headers + log;
    }
    fs.appendFileSync(logfile, log);
  });
}

module.exports = predictionsLogger;
